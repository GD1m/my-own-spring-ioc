package org.springframework.beans.factory;

import com.sun.media.sound.InvalidDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

public final class BeanFactory {
    private Map<String, Object> singletons = new HashMap<>();

    private List<BeanPostProcessor> postProcessors = new ArrayList<>();

    public Map<String, Object> getSingletons() {
        return singletons;
    }

    public Object getBean(String beanName) {
        return singletons.get(beanName);
    }

    public BeanFactory addPostProcessor(BeanPostProcessor postProcessor) {
        postProcessors.add(postProcessor);

        return this;
    }

    public BeanFactory instantiate(String packageName) throws
            IOException,
            URISyntaxException,
            ClassNotFoundException,
            InstantiationException,
            IllegalAccessException {
        Enumeration<URL> resources = getURLResources(packageName);

        while (resources.hasMoreElements()) {
            processURLResource(resources.nextElement(), packageName);
        }

        return this;
    }

    public BeanFactory populateProperties() throws
            NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException {
        for (Object instance : singletons.values()) {
            for (Field field : instance.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Autowired.class)) {
                    for (Object dependency : singletons.values()) {
                        if (dependency.getClass().equals(field.getType())) {
                            injectDependency(instance, field, dependency);
                        }
                    }
                }
            }
        }

        return this;
    }

    public BeanFactory injectBeanProperties() throws ClassCastException {
        for (String beanName : singletons.keySet()) {
            Object bean = singletons.get(beanName);

            if (bean instanceof BeanNameAware) {
                ((BeanNameAware) bean).setBeanName(beanName);
            }

            if (bean instanceof BeanFactoryAware) {
                ((BeanFactoryAware) bean).setBeanFactory(this);
            }
        }

        return this;
    }

    public BeanFactory initializeBeans() throws InvocationTargetException, IllegalAccessException, ClassCastException {
        for (String beanName : singletons.keySet()) {
            Object bean = singletons.get(beanName);

            for (BeanPostProcessor postProcessor : postProcessors) {
                postProcessor.postProcessBeforeInitialization(bean, beanName);
            }

            if (bean instanceof InitializingBean) {
                ((InitializingBean) bean).afterPropertiesSet();
            }

            for (BeanPostProcessor postProcessor : postProcessors) {
                postProcessor.postProcessAfterInitialization(bean, beanName);
            }
        }

        return this;
    }

    public void close() throws InvocationTargetException, IllegalAccessException, ClassCastException {
        for (Object bean : singletons.values()) {
            for (Method method : bean.getClass().getDeclaredMethods()) {
                if (method.isAnnotationPresent(PreDestroy.class)) {
                    method.invoke(bean);
                }
            }

            if (bean instanceof DisposableBean) {
                ((DisposableBean) bean).destroy();
            }
        }
    }

    private void injectDependency(Object instance, Field field, Object dependency) throws
            NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException {
        StringBuilder stringBuilder = new StringBuilder();

        String fieldName = field.getName();

        stringBuilder.append("set");
        stringBuilder.append(fieldName.substring(0, 1).toUpperCase());
        stringBuilder.append(fieldName.substring(1));

        String setterName = stringBuilder.toString();

        Method setter = instance.getClass().getMethod(setterName, dependency.getClass());

        setter.invoke(instance, dependency);
    }

    private Enumeration<URL> getURLResources(String packageName) throws IOException {
        String path = packageName.replace('.', '/');

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        return classLoader.getResources(path);
    }

    private void processURLResource(URL resource, String packageName) throws
            URISyntaxException,
            IOException,
            ClassNotFoundException,
            IllegalAccessException,
            InstantiationException {
        File directory = new File(resource.toURI());

        if (!directory.exists()) {
            throw new InvalidDataException("Directory not found: " + resource.toString());
        }

        if (!directory.canRead()) {
            throw new RuntimeException("Can not read the directory (Permission denied): " + resource.toString());
        }

        if (directory.isFile()) {
            processFile(directory, packageName);
        }

        File[] fileList = directory.listFiles();

        if (null == fileList) {
            return;
        }

        processFileList(fileList, packageName);
    }

    private void processFileList(File[] fileList, String packageName) throws
            IOException,
            URISyntaxException,
            ClassNotFoundException,
            InstantiationException,
            IllegalAccessException {
        for (File file : fileList) {
            if (file.isDirectory()) {
                instantiate(packageName + "." + file.getName());
            }

            processFile(file, packageName);
        }
    }

    private void processFile(File file, String packageName) throws
            ClassNotFoundException,
            IllegalAccessException,
            InstantiationException {
        if (!file.canRead()) {
            throw new RuntimeException("Can not read the file (Permission denied): " + file.toPath());
        }

        if (!isClass(file)) {
            return;
        }

        Class classObject = getClassObject(file, packageName);

        if (itShouldBeInstantiated(classObject)) {
            Object instance = makeInstance(classObject);

            String beanName = getBeanName(classObject);

            singletons.put(beanName, instance);
        }
    }

    private Boolean isClass(File file) {
        return file.isFile() && file.getName().endsWith(".class");
    }

    private Class getClassObject(File file, String packageName) throws ClassNotFoundException {
        String className = getClassName(file);

        return Class.forName(packageName + "." + className);
    }

    private String getClassName(File file) {
        String fileName = file.getName();

        return fileName.substring(0, fileName.lastIndexOf('.'));
    }

    private boolean itShouldBeInstantiated(Class classObject) {
        return null != classObject && classObject.isAnnotationPresent(Component.class);
    }

    private Object makeInstance(Class classObject) throws InstantiationException, IllegalAccessException {
        return classObject.newInstance();
    }

    private String getBeanName(Class classObject) {
        String className = classObject.getSimpleName();

        return className.substring(0, 1).toLowerCase() + className.substring(1);
    }
}