package org.springframework.context.eventdispatcher;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public final class ApplicationEventDispatcher {
    private BeanFactory beanFactory;

    public ApplicationEventDispatcher(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public void dispatch(Class classObject) throws
            NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException,
            InstantiationException {
        for (Object bean : beanFactory.getSingletons().values()) {
            if (bean instanceof ApplicationListener) {
                for (Type type : bean.getClass().getGenericInterfaces()) {
                    if (type instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) type;

                        Type genericParameter = parameterizedType.getActualTypeArguments()[0];

                        if (genericParameter.equals(classObject)) {
                            Method method = bean.getClass().getMethod("onApplicationEvent", classObject);

                            method.invoke(bean, classObject.newInstance());
                        }
                    }
                }
            }
        }
    }
}
