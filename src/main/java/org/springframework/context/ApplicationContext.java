package org.springframework.context;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.postprocessor.PostConstructAnnotationBeanPostProcessor;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.eventdispatcher.ApplicationEventDispatcher;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

public final class ApplicationContext {
    private BeanFactory beanFactory = new BeanFactory();

    private ApplicationEventDispatcher applicationEventDispatcher = new ApplicationEventDispatcher(beanFactory);

    public ApplicationContext(String packageName) throws
            URISyntaxException,
            ClassNotFoundException,
            InstantiationException,
            IllegalAccessException,
            IOException,
            NoSuchMethodException,
            InvocationTargetException {
        beanFactory
                .addPostProcessor(new PostConstructAnnotationBeanPostProcessor())
                .instantiate(packageName)
                .populateProperties()
                .injectBeanProperties()
                .initializeBeans();
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public void close() throws
            InvocationTargetException,
            IllegalAccessException,
            NoSuchMethodException,
            InstantiationException {
        beanFactory.close();

        dispatchEvent(ContextClosedEvent.class);
    }

    private void dispatchEvent(Class classObject) throws
            NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException,
            InstantiationException {
        applicationEventDispatcher.dispatch(classObject);
    }
}
