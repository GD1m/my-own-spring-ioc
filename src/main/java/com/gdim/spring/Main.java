package com.gdim.spring;

import com.gdim.spring.service.cat.Cat;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) throws
            IllegalAccessException,
            InvocationTargetException,
            InstantiationException,
            IOException,
            URISyntaxException,
            NoSuchMethodException,
            ClassNotFoundException,
            ClassCastException {
        ApplicationContext applicationContext = new ApplicationContext("com.gdim.spring");

        BeanFactory beanFactory = applicationContext.getBeanFactory();

        Cat cat = (Cat) beanFactory.getBean("cat");

        System.out.println(cat);

        applicationContext.close();
    }
}