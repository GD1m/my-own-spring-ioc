package com.gdim.spring.service.cat.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.stereotype.Component;

import java.util.Objects;

@Component
public final class Head {
    @Autowired
    private Eye leftEye;

    @Autowired
    private Eye rightEye;

    public void setLeftEye(Eye leftEye) {
        this.leftEye = leftEye;
    }

    public void setRightEye(Eye rightEye) {
        this.rightEye = rightEye;
    }

    @Override
    public String toString() {
        return "Head{" +
                "leftEye=" + leftEye +
                ", rightEye=" + rightEye +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Head head = (Head) o;

        return Objects.equals(leftEye, head.leftEye) &&
                Objects.equals(rightEye, head.rightEye);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftEye, rightEye);
    }
}
