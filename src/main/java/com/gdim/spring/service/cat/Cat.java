package com.gdim.spring.service.cat;

import com.gdim.spring.service.cat.modules.Head;
import com.gdim.spring.service.cat.modules.Tail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public final class Cat {
    @Autowired
    private Head head;

    @Autowired
    private Tail tail;

    public void setHead(Head head) {
        this.head = head;
    }

    public void setTail(Tail tail) {
        this.tail = tail;
    }

    @PostConstruct
    public void hello() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Cat{" +
                "head=" + head +
                ", tail=" + tail +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Cat cat = (Cat) o;

        return Objects.equals(head, cat.head) &&
                Objects.equals(tail, cat.tail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, tail);
    }
}
